"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require("fs");
var path = require("path");
var canvas_1 = require("canvas");
var generateGradient = function (imagePath_1) {
    var args_1 = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        args_1[_i - 1] = arguments[_i];
    }
    return __awaiter(void 0, __spreadArray([imagePath_1], args_1, true), void 0, function (imagePath, numPoints) {
        var img, canvas, context, colors, step, i, x, y, pixelData, color;
        if (numPoints === void 0) { numPoints = 5; }
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, (0, canvas_1.loadImage)(imagePath)];
                case 1:
                    img = _a.sent();
                    canvas = (0, canvas_1.createCanvas)(img.width, img.height);
                    context = canvas.getContext('2d');
                    context.drawImage(img, 0, 0);
                    colors = [];
                    step = img.width / (numPoints - 1);
                    for (i = 0; i < numPoints; i++) {
                        x = Math.floor(step * i);
                        y = Math.floor(img.height / 2);
                        pixelData = context.getImageData(x, y, 1, 1).data;
                        color = "rgb(".concat(pixelData[0], ", ").concat(pixelData[1], ", ").concat(pixelData[2], ")");
                        colors.push(color);
                    }
                    return [2 /*return*/, "linear-gradient(to right, ".concat(colors.join(', '), ")")];
            }
        });
    });
};
var processDirectory = function (dirPath, baseDir) { return __awaiter(void 0, void 0, void 0, function () {
    var structure, items, _i, items_1, item, itemPath, relativePath, stat, _a, _b, gradient;
    return __generator(this, function (_c) {
        switch (_c.label) {
            case 0:
                structure = {};
                items = fs.readdirSync(dirPath);
                _i = 0, items_1 = items;
                _c.label = 1;
            case 1:
                if (!(_i < items_1.length)) return [3 /*break*/, 6];
                item = items_1[_i];
                itemPath = path.join(dirPath, item);
                relativePath = path.relative(baseDir, itemPath);
                stat = fs.statSync(itemPath);
                if (!stat.isDirectory()) return [3 /*break*/, 3];
                _a = structure;
                _b = item;
                return [4 /*yield*/, processDirectory(itemPath, baseDir)];
            case 2:
                _a[_b] = _c.sent();
                return [3 /*break*/, 5];
            case 3:
                if (!/\.(jpg|jpeg|png|gif)$/.test(item)) return [3 /*break*/, 5];
                return [4 /*yield*/, generateGradient(itemPath)];
            case 4:
                gradient = _c.sent();
                structure[item] = {
                    gradient: gradient,
                    path: "$assets/".concat(relativePath.replace(/\\/g, '/')),
                };
                _c.label = 5;
            case 5:
                _i++;
                return [3 /*break*/, 1];
            case 6: return [2 /*return*/, structure];
        }
    });
}); };
var generateGradientsFile = function () { return __awaiter(void 0, void 0, void 0, function () {
    var assetsDir, outputDir, outputFile, structure, content;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                assetsDir = path.join(__dirname, 'src', 'assets');
                outputDir = path.join(__dirname, 'src', 'generated');
                outputFile = path.join(outputDir, 'gradients.ts');
                return [4 /*yield*/, processDirectory(assetsDir, assetsDir)];
            case 1:
                structure = _a.sent();
                content = "export const gradients = ".concat(JSON.stringify(structure, null, 2), ";\n");
                fs.mkdirSync(outputDir, { recursive: true });
                fs.writeFileSync(outputFile, content);
                return [2 /*return*/];
        }
    });
}); };
generateGradientsFile()
    .then(function () {
    console.log('Gradients TS file generated successfully.');
})
    .catch(function (error) {
    console.error('Error generating gradients:', error);
});
