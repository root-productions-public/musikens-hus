import { join, resolve, relative } from 'path'
import * as fs from 'fs'
import { createCanvas, loadImage, type Image, type CanvasRenderingContext2D } from 'canvas'

// Get command-line arguments
const [, , assetsPathArg, outputPathArg] = process.argv

// Resolve paths relative to the current working directory
const assetsDir = resolve(process.cwd(), assetsPathArg || 'src/assets')
const outputDir = resolve(process.cwd(), outputPathArg || 'src/generated')

const drawImage = async (imagePath: string) => {
	const image = await loadImage(imagePath)
	const canvas = createCanvas(image.width, image.height)
	const context = canvas.getContext('2d')

	context.drawImage(image, 0, 0)

	return { image, context }
}

const generateSingleLinearGradient =
	(numPoints = 8) =>
	(context: CanvasRenderingContext2D, image: Image): string => {
		const colors: string[] = []
		const step = image.width / (numPoints - 1)

		// Get the pixel data for the entire image
		const imageData = context.getImageData(0, 0, image.width, image.height).data

		for (let i = 0; i < numPoints; i++) {
			const x = Math.floor(step * i)
			const y = Math.floor(image.height / 2) // Sample from the middle height

			const index = (y * image.width + x) * 4
			const red = imageData[index]
			const green = imageData[index + 1]
			const blue = imageData[index + 2]

			const color = `rgba(${red}, ${green}, ${blue}, 0.5)`
			colors.push(color)
		}

		return `linear-gradient(to right, ${colors.join(', ')})`
	}

const generateSingleLinearGradientTopToBottom =
	(numPoints = 8) =>
	(context: CanvasRenderingContext2D, image: Image): string => {
		const colors: string[] = []
		const step = image.height / (numPoints - 1)

		const imageData = context.getImageData(0, 0, image.width, image.height).data

		for (let i = 0; i < numPoints; i++) {
			const y = Math.floor(step * i)
			const x = Math.floor(image.width / 2)

			const index = (x * image.width + y) * 4
			const red = imageData[index]
			const green = imageData[index + 1]
			const blue = imageData[index + 2]

			const color = `rgba(${red}, ${green}, ${blue}, 0.5)`
			colors.push(color)
		}

		return `linear-gradient(to bottom, ${colors.join(', ')})`
	}

const generateDiagonalGradientTopRight =
	(numPoints = 8) =>
	(context: CanvasRenderingContext2D, image: Image): string => {
		const colors: string[] = []
		const stepX = image.width / (numPoints - 1)
		const stepY = image.height / (numPoints - 1)

		const imageData = context.getImageData(0, 0, image.width, image.height).data

		for (let i = 0; i < numPoints; i++) {
			const x = Math.floor(stepX * i)
			const y = Math.floor(stepY * i)

			// Ensure x and y are within image bounds and use correct index calculation
			if (x >= 0 && x < image.width && y >= 0 && y < image.height) {
				const index = (y * image.width + x) * 4
				const red = imageData[index]
				const green = imageData[index + 1]
				const blue = imageData[index + 2]

				const color = `rgba(${red}, ${green}, ${blue}, 0.5)`
				colors.push(color)
			} else {
				colors.push(`rgba(0, 0, 0, 0.5)`) // Default color for out-of-bounds
			}
		}

		return `linear-gradient(to top right, ${colors.join(', ')})`
	}

const generateDiagonalGradientBottomLeft =
	(numPoints = 8) =>
	(context: CanvasRenderingContext2D, image: Image): string => {
		const colors: string[] = []
		const stepX = image.width / (numPoints - 1)
		const stepY = image.height / (numPoints - 1)

		const imageData = context.getImageData(0, 0, image.width, image.height).data

		for (let i = 0; i < numPoints; i++) {
			const x = Math.floor(stepX * i)
			const y = Math.floor(image.height - stepY * i)

			// Ensure x and y are within image bounds and use correct index calculation
			if (x >= 0 && x < image.width && y >= 0 && y < image.height) {
				const index = (y * image.width + x) * 4
				const red = imageData[index]
				const green = imageData[index + 1]
				const blue = imageData[index + 2]

				const color = `rgba(${red}, ${green}, ${blue}, 0.5)`
				colors.push(color)
			} else {
				colors.push(`rgba(0, 0, 0, 0.5)`) // Default color for out-of-bounds
			}
		}

		return `linear-gradient(to bottom left, ${colors.join(', ')})`
	}

const processDirectory = async (dirPath: string, baseDir: string): Promise<Record<string, any>> => {
	const structure: Record<string, any> = {}
	const items = fs.readdirSync(dirPath)

	for (const item of items) {
		const itemPath = join(dirPath, item)
		const relativePath = relative(baseDir, itemPath)
		const stat = fs.statSync(itemPath)

		if (stat.isDirectory()) {
			structure[item] = await processDirectory(itemPath, baseDir)
		} else if (/\.(jpg|jpeg|png|gif)$/.test(item)) {
			const { context, image } = await drawImage(itemPath)

			const singleLinearGradient = generateSingleLinearGradient()(context, image)
			const singleLinearGradientTopToBottom = generateSingleLinearGradientTopToBottom()(
				context,
				image,
			)
			const diagonalGradientTopRight = generateDiagonalGradientTopRight()(context, image)
			const diagonalGradientBottomLeft = generateDiagonalGradientBottomLeft()(context, image)

			// Combine all gradients into a background string
			const combinedGradient = `background: ${singleLinearGradient}, ${singleLinearGradientTopToBottom}, ${diagonalGradientTopRight}, ${diagonalGradientBottomLeft}; background-blend-mode: multiply;`

			structure[item] = {
				combinedGradient,
				path: `$assets/${relativePath.replace(/\\/g, '/')}`,
			}
		}
	}

	return structure
}

const generateGradientsFile = async () => {
	const outputFile = join(outputDir, 'gradients.ts')

	const structure = await processDirectory(assetsDir, assetsDir)

	const content = `export const gradients = ${JSON.stringify(structure, null, 2)};\n`

	fs.mkdirSync(outputDir, { recursive: true })
	fs.writeFileSync(outputFile, content)
}

generateGradientsFile()
	.then(() => {
		console.log('Gradients TS file generated successfully.')
	})
	.catch((error) => {
		console.error('Error generating gradients:', error)
	})
