import FacebookLink from './facebook-link.svelte'
import InstagramLink from './instagram-link.svelte'
import PatreonLink from './patreon-link.svelte'
import YoutubeLink from './youtube-link.svelte'
import ButtonLink from './button-link.svelte'
import SocialLink from './social-link.svelte'

export {
  ButtonLink,
  FacebookLink,
  InstagramLink,
  PatreonLink,
  YoutubeLink,
  SocialLink
}
export type { Link, LinkType } from './social-link'
