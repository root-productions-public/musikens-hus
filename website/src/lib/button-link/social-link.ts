type LinkType = 'facebook' | 'instagram' | 'patreon' | 'youtube' | 'home' | 'other'

type Link = {
  url: string
  text?: string
  type: LinkType
}

export type { Link, LinkType }
