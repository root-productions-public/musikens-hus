const capitalize = (from: number, to: number) => (str: string): string => {
  if (from >= 0 && to <= str.length) {
    return str.slice(0, from) + str.slice(from, to).toUpperCase() +
      str.slice(to);
  }
  return str; // If the range is invalid, return the string as is
};

export { capitalize };
