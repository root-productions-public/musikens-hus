import {
	differenceInMonths,
	differenceInDays,
	differenceInHours,
	differenceInMinutes,
	addMonths,
	addDays,
	addHours,
	addMinutes,
	differenceInSeconds
} from 'date-fns'

const calculateDateDifference = (fromDate: Date, toDate: Date) => {
	const monthDifference = differenceInMonths(toDate, fromDate)
	const dateAfterMonthDifference = addMonths(fromDate, monthDifference)
	const dayDifference = differenceInDays(toDate, dateAfterMonthDifference)
	const dateAfterDayDifference = addDays(dateAfterMonthDifference, dayDifference)
	const hourDifference = differenceInHours(toDate, dateAfterDayDifference)
	const dateAfterHourDifference = addHours(dateAfterDayDifference, hourDifference)
	const minuteDifference = differenceInMinutes(toDate, dateAfterHourDifference)
	const dateAfterMinuteDifference = addMinutes(dateAfterHourDifference, minuteDifference)
	const secondDifference = differenceInSeconds(toDate, dateAfterMinuteDifference)

	return {
		months: monthDifference,
		days: dayDifference,
		hours: hourDifference,
		minutes: minuteDifference,
		seconds: secondDifference
	}
}

export { calculateDateDifference }
