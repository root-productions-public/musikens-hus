import { fromZonedTime } from 'date-fns-tz';

type Year = `${19 | 20}${number}${number}`;
type Month = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12;

const padZero = (n: number) => (n < 10 ? `0${n}` : `${n}`);

const createDate =
  (year: Year) => (month: Month) => (day: number) => (time: string) =>
    new Date(`${year}-${padZero(month)}-${padZero(day)}T${time}:00`);

const createDateForTimeZone = (timeZone: string) =>
  (year: Year) => (month: Month) => (day: number) => (time: string) => {
    const dateString = `${year}-${padZero(month)}-${padZero(day)}T${time}:00`;
    // Convert to correct UTC Date object from the given time zone
    const zonedDate = fromZonedTime(dateString, timeZone);
    return zonedDate; // This should return the correct UTC Date object
  };

export { createDate, createDateForTimeZone };
