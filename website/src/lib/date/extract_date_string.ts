const extractDateString = (date: Date): string => date.toISOString().split('T')[0]

export { extractDateString }
