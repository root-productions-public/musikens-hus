import { extractDateString } from './extract_date_string';

const groupByDay =
  <TTimeRange>(
    getStart: (range: TTimeRange) => Date,
    getEnd: (range: TTimeRange) => Date,
    shiftFn?: (date: Date) => Date,
  ) =>
  <TExtendedTimeRange extends TTimeRange>(
    timeRanges: TExtendedTimeRange[],
  ): Record<string, TExtendedTimeRange[]> => {
    const groups: Record<string, TExtendedTimeRange[]> = {};

    for (const range of timeRanges) {
      // Create new date instances before applying any shift to avoid mutating the original dates
      const startDate = new Date(getStart(range).getTime());
      const endDate = new Date(getEnd(range).getTime());

      // Destructure adjustedStartTime and adjustedEndTime in one ternary check for shiftFn
      const [adjustedStartTime, adjustedEndTime] = shiftFn
        ? [shiftFn(startDate), shiftFn(endDate)]
        : [startDate, endDate];

      const startDayKey = extractDateString(adjustedStartTime);
      const endDayKey = extractDateString(adjustedEndTime);

      if (!groups[startDayKey]) {
        groups[startDayKey] = [];
      }
      groups[startDayKey].push(range);

      if (startDayKey !== endDayKey) {
        if (!groups[endDayKey]) {
          groups[endDayKey] = [];
        }
        groups[endDayKey].push(range);
      }
    }

    return groups;
  };

export { groupByDay };
