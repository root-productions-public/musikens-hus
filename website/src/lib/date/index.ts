import { groupByDay } from './group_by_day'
import { extractDateString } from './extract_date_string'
import { shiftToCustomDay } from './shift_to_custom_day'
import { sortGroupedByDate } from './sort_grouped_by_date'

export { groupByDay, extractDateString, shiftToCustomDay, sortGroupedByDate }

// Example usage with a different structure
type CustomRange = {
	date: {
		start: Date
		end: Date
	}
	description: string // Additional property
}

// Custom time ranges with additional properties
const customTimeRanges: CustomRange[] = [
	{
		date: { start: new Date('2024-09-10T07:00:00Z'), end: new Date('2024-09-10T10:00:00Z') },
		description: 'Event 1',
	},
	{
		date: { start: new Date('2024-09-10T04:00:00Z'), end: new Date('2024-09-10T06:30:00Z') },
		description: 'Event 2',
	},
	{
		date: { start: new Date('2024-09-09T23:00:00Z'), end: new Date('2024-09-10T02:00:00Z') },
		description: 'Event 3',
	},
]

// Custom getter functions for the "start" and "end" properties
const getCustomStart = (range: CustomRange): Date => range.date.start
const getCustomEnd = (range: CustomRange): Date => range.date.end

// Group without shift (default behavior)
const groupedWithoutShift = groupByDay(getCustomStart, getCustomEnd)(customTimeRanges)
const sortedGroupedWithoutShift = sortGroupedByDate(groupedWithoutShift)
console.log('Grouped Without Shift (Sorted):', sortedGroupedWithoutShift)

// Group with a shift of 6 hours
const shiftBySixHours = shiftToCustomDay(6)
const groupedWithShift = groupByDay(getCustomStart, getCustomEnd, shiftBySixHours)(customTimeRanges)
const sortedGroupedWithShift = sortGroupedByDate(groupedWithShift)
console.log('Grouped With 6-hour Shift (Sorted):', sortedGroupedWithShift)
