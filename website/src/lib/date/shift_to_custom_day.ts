const shiftToCustomDay =
	(hoursToShift: number) =>
	(date: Date): Date =>
		new Date(date.setHours(date.getHours() - hoursToShift))

export { shiftToCustomDay }
