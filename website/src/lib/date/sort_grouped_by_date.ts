const sortGroupedByDate = <TTimeRange>(
	groupedData: Record<string, TTimeRange[]>,
): [string, TTimeRange[]][] =>
	Object.entries(groupedData).sort(
		([dateA], [dateB]) => new Date(dateA).getTime() - new Date(dateB).getTime(),
	)

export { sortGroupedByDate }
