import { addDays, isValid } from 'date-fns'

const environment = () => {
	const dateTimeOpening = new Date(import.meta.env.VITE_PUBLIC_DATE_TIME_OPENING)

	return {
		dateTimeOpening: isValid(dateTimeOpening) ? dateTimeOpening : addDays(new Date(), 1),
		facebookUrl: import.meta.env.VITE_PUBLIC_FACEBOOK_URL ?? '/',
		instagramUrl: import.meta.env.VITE_PUBLIC_INSTAGRAM_URL ?? '/',
		patreonUrl: import.meta.env.VITE_PUBLIC_PATREON_URL ?? '/',
    youtubeUrl: import.meta.env.VITE_PUBLIC_YOUTUBE_URL ?? '/'
	}
}

export { environment }
