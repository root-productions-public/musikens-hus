import { format } from 'date-fns'
import { formatInTimeZone } from 'date-fns-tz'
import { sv } from 'date-fns/locale'
import { capitalize } from '$lib/capitalize'

const textFormat = {
  dateDay: 'dd',
  dateMonth: 'MM',
  dateYear: 'yyyy',
  fullDate: 'yyyy-MM-dd',
  fullDateAndTime: 'yyyy-MM-dd HH:mm',
  time: 'HH:mm'
} as const

const stockholmTimeZone = 'Europe/Stockholm'

/**
 * Holds methods used to format `dateTime`.
 *
 * Use this to avoid hard coding dateTime formats.
 *
 */
const formatTemporal = {
  /** Formats `dateTime` as `yyyy-MM-dd` */
  fullDate: (dateTime: number | Date) => format(dateTime, textFormat.fullDate),
  /** Formats `dateTime` as `yyyy-MM-dd HH:mm` */
  fullDateAndTime: (dateTime: number | Date) => format(dateTime, textFormat.fullDateAndTime),
  /** Formats `dateTime` as `HH:mm` */
  time: (dateTime: number | Date) =>
    formatInTimeZone(dateTime, stockholmTimeZone, 'HH:mm', { locale: sv })
  ,
  /** Prefer specified options over custom */
  custom: (dateTime: number | Date, how: string) => format(dateTime, how),
  weekdayInMonth: (dateTime: Date): string => {
    const formattedDate = formatInTimeZone(dateTime, stockholmTimeZone, 'EEEE dd MMMM', {
      locale: sv,
    })

    const [dayOfWeek, dayOfMonth, month] = formattedDate.split(' ')

    return `${capitalize(0, 1)(dayOfWeek)} ${dayOfMonth} ${capitalize(0, 1)(month)}`
  },
  /** The text used as format in methods */
  textFormat
}

export { formatTemporal }
