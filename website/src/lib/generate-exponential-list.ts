const generateExponentialList = (
	from: number,
	to: number,
	length: number,
	exponent: number = 2,
): number[] => {
	const result: number[] = []
	const step = 1 / (length - 1)

	for (let i = 0; i < length; i++) {
		const ratio = i * step // Normalized ratio from 0 to 1
		const value = from + (to - from) * Math.pow(ratio, exponent)
		result.push(value)
	}

	return result
}

export { generateExponentialList }
