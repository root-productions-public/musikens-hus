type Price = {
	withoutTax: number
	tax: number
	withTax: number
}

const membershipTypes = ['networker', 'creator'] as const

type MembershipType = (typeof membershipTypes)[number]

type Membership<TMembershipType extends MembershipType> = {
	type: TMembershipType
	name: string
	emoji: string
	price: Price
	link: string
	description: {
		medium: string
	}
}

const createPrice = (withoutTax: number): Price => {
	const withTax = withoutTax * 1.25
	return {
		withoutTax,
		tax: withTax - withoutTax,
		withTax,
	}
}

const createMembership = <TMembershipType extends MembershipType>({
	type,
	name,
	emoji,
	/** Without tax */
	price,
	description,
}: {
	type: TMembershipType
	name: string
	emoji: string
	/** Without tax */
	price: number
	description: string
}): Membership<TMembershipType> => {
	return {
		type,
		name,
		emoji,
		price: createPrice(price),
		link: `/membership/${type}`,
		description: {
			medium: description,
		},
	}
}

const memberships = [
	createMembership({
		type: 'networker',
		name: 'Networker',
		emoji: '🧑‍💻',
		price: 1500,
		description: 'Fri tillgång till husets öppna ytor och en mindre studio.',
	}),
	createMembership({
		type: 'creator',
		name: 'Creator',
		emoji: '🧙‍♂️',
		price: 2500,
		description: 'Fri tillgång till husets öppna ytor och samtliga kreativa rum.',
	}),
] as const

export { memberships, type Price, type Membership, type MembershipType }
