import type { UsersSolid } from 'flowbite-svelte-icons'

type Link = {
	path: string
	displayName: string
	icon: typeof UsersSolid
}

export type { Link }
