import {
	HomeSolid,
	MessageDotsSolid,
	UsersSolid,
	CalendarMonthOutline,
	DesktopPcOutline,
	InfoCircleOutline,
} from 'flowbite-svelte-icons'
/* TODO: Maybe add this icon library instead
https://lucide.dev/icons/?search=note
*/
import type { Link } from './link-type'
import { navigation } from '$lib/navigation'

const topNavLinks: Link[] = [
	{ path: navigation.home, displayName: 'Hem', icon: HomeSolid },
	{ path: navigation.membership._root, displayName: 'Coworking', icon: UsersSolid },
	{ path: navigation.events._root, displayName: 'Event', icon: MessageDotsSolid },
	{
		path: navigation.musicProduction._root,
		displayName: 'Produktion',
		icon: MessageDotsSolid,
	},
	{ path: navigation.about, displayName: 'Om oss', icon: MessageDotsSolid },
	{ path: navigation.contact, displayName: 'Kontakt', icon: MessageDotsSolid },
]

const bottomNavLinks: Link[] = [
	{ path: navigation.membership._root, displayName: 'Coworking', icon: UsersSolid },
	{ path: navigation.home, displayName: 'Hem', icon: HomeSolid },
	{ path: navigation.contact, displayName: 'Kontakt', icon: MessageDotsSolid },
]

const sideNavLinks: Link[] = [
	{ path: navigation.about, displayName: 'Om oss', icon: InfoCircleOutline },
	{ path: navigation.events._root, displayName: 'Event', icon: CalendarMonthOutline },
	{
		path: navigation.musicProduction._root,
		displayName: 'Produktion',
		icon: DesktopPcOutline,
	},
]

const links = {
	topNav: topNavLinks,
	bottomNav: bottomNavLinks,
	sideNav: sideNavLinks,
}

export { links }
