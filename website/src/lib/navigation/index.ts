type Mutable<T> = {
	-readonly [K in keyof T]: Mutable<T[K]>
}

const asMutable = <T extends Record<string, unknown>>(obj: T): Mutable<T> => obj

const membership = {
	_root: '/membership',
	networker: `/membership/networker`,
	creator: `/membership/creator`,
} as const

const events = {
	_root: '/events',
}

const musicProduction = {
	_root: '/music-production',
}

const navigation = {
	home: '/',
	membership,
	contact: '/contact',
	about: '/about',
	events,
	musicProduction,
} as const

export { navigation }
