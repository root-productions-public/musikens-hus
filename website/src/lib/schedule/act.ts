type Act = {
	name: string
	location: string
	startTime: Date
	endTime: Date
	url?: string
}

export type { Act }
