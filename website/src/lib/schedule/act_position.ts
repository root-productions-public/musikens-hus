import type { Act } from './act'

const actStartFractionOfDuration = (act: Act, start: Date, end: Date): number => {
	const startTimeMs = start.getTime() // Schedule's start time in ms
	const totalDuration = end.getTime() - startTimeMs // Total duration in ms (across all days)
	const actStartTime = act.startTime.getTime() // Act's start time in ms

	const timeDifference = actStartTime - startTimeMs // Difference from the start

	// Percentage position relative to the total duration
	return timeDifference / totalDuration
}

const actHeightFractionOfDuration = (act: Act, start: Date, end: Date): number => {
	const totalDuration = end.getTime() - start.getTime() // Total duration in ms (across all days)
	const actDuration = act.endTime.getTime() - act.startTime.getTime() // Act's duration in ms

	// Calculate percentage height relative to the total duration
	return actDuration / totalDuration
}

export { actHeightFractionOfDuration, actStartFractionOfDuration }
