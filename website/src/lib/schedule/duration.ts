type Duration = {
  start: Date
  end: Date
}

export type { Duration }
