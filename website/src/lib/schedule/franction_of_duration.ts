import type { Duration } from "./duration"

const fractionOfDuration = (time: Date, duration: Duration): number => {
  const startTimeMs = duration.start.getTime()
  const totalDurationMs = duration.end.getTime() - startTimeMs

  const timeMs = time.getTime()

  const timeDifferenceMs = timeMs - startTimeMs

  return timeDifferenceMs / totalDurationMs
}

export { fractionOfDuration }
