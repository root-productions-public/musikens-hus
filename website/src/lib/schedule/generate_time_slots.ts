import { roundToPreviousInterval } from './round_to_previous_interval'
import type { Duration } from './duration'

const generateTimeSlots =
  (timeSlotMinutes: number) =>
    (start: Date, end: Date): Duration[] => {
      const times: Duration[] = []
      let currentTime = new Date(roundToPreviousInterval(timeSlotMinutes)(start))

      while (currentTime < end) {
        const newDate = new Date(currentTime)
        newDate.setMinutes(newDate.getMinutes() + timeSlotMinutes)
        // Add start and end time for each slot
        times.push({ start: new Date(currentTime), end: newDate })

        currentTime = newDate
      }
      return times
    }

export { generateTimeSlots }
