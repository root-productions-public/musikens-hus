import type { Act } from './act'

const locationsFromActs = (acts: Act[]) => {
	let locationsSet = new Set<string>()
	acts.forEach((act) => locationsSet.add(act.location))
	return [...locationsSet].toSorted()
}

export { locationsFromActs }
