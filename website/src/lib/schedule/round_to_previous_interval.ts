const roundToPreviousInterval =
	(interval: number) =>
	(date: Date): Date => {
		const minutes = date.getMinutes()
		const newMinutes = Math.floor(minutes / interval) * interval
		const newDate = new Date(date)
		newDate.setMinutes(newMinutes, 0, 0) // Set minutes and reset seconds and milliseconds to zero
		return newDate
	}

const roundToPrevious30Minutes = roundToPreviousInterval(30)

export { roundToPreviousInterval, roundToPrevious30Minutes }
