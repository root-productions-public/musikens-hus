type TimeSlot = {
	start: Date
	end: Date
}

export type { TimeSlot }
