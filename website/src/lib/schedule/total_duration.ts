import type { Duration } from "./duration"

const totalDuration = (durations: Duration[]): Duration | null => {
  if (durations.length === 0) {
    return null
  }

  return {
    start: durations[0].start,
    end: durations[durations.length - 1].end,
  }
}

export { totalDuration }
