const load = async ({ url }) => {
	return {
		url: url.pathname,
	}
}

export { load }
