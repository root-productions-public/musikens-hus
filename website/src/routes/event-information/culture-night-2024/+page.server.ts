import { acts } from './act-data';

export const load = async () => {
  return { acts };
};
