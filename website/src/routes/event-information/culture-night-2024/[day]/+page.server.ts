import { groupByDay, shiftToCustomDay } from '$lib/date';
import { acts, type Act } from '../act-data';

export const load = async ({ params }) => {
  const { day } = params as { day: string };

  const shiftBySixHours = shiftToCustomDay(6)

  const groupedByDay = groupByDay<Act>(
    (r) => r.startTime,
    (r) => r.endTime,
    shiftBySixHours,
  )(acts)

  const actsOnDay = groupedByDay[day] ?? []

  return {
    day,
    acts: actsOnDay,
  };
};
