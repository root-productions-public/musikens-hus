import type { Link } from '$lib/button-link'
import { capitalize } from '$lib/capitalize'
import { createDateForTimeZone } from '$lib/date/create_date'

type Venue = 'Marknadstorget' | 'Restaurangtorget' | 'Brasseriet'

type Act = {
  name: string
  location: Venue
  startTime: Date
  endTime: Date
  id: string
  url?: string
  image?: string
  hideHeader: boolean
  description?: string[]
  links: Link[]
}

const createDate = createDateForTimeZone('Europe/Stockholm')

const dayInSeptember = createDate('2024')(9)
const cT27 = dayInSeptember(27)
const cT28 = dayInSeptember(28)
const cT29 = dayInSeptember(29)

const imageDirectory = '/src/assets/event-information/culture-night-2024'

const imageModules = import.meta.glob(
  '/src/assets/event-information/culture-night-2024/*.{avif,gif,heif,jpeg,jpg,png,tiff,webp,svg}',
  {
    eager: true,
    query: {
      enhanced: true,
    },
  },
)

const addImage = (path: string) =>
  (imageModules[`${imageDirectory}/${path}`] as { default?: string })?.default ?? undefined

const createAct =
  (location: Venue) =>
    (
      name: string,
      startTime: Date,
      endTime: Date,
      id: string,
      optional: { image?: string; description?: string[]; links?: Link[]; hideHeader?: boolean } = {},
    ): Act => ({
      name,
      location,
      startTime,
      endTime,
      id,
      url: `/event-information/culture-night-2024/acts/${id}`,
      image: optional.image && addImage(optional.image),
      description: optional.description,
      hideHeader: optional.hideHeader ?? false,
      links: optional.links ?? [],
    })

const mAct = createAct('Marknadstorget')
const rAct = createAct('Restaurangtorget')
const bAct = createAct('Brasseriet')

const createLink = (type: Link['type']) => (url: string) => ({
  url,
  type,
  text: type === 'other' || type === 'home' ? 'Hemsida' : capitalize(0, 1)(type),
})
const cLInstagram = createLink('instagram')
const cLFacebook = createLink('facebook')
const cLPatreon = createLink('patreon')
const cLYoutube = createLink('youtube')
const cLHome = createLink('home')
const cLOther = createLink('other')

const acts: Act[] = [
  mAct('Jonas Jansson', cT27('16:00'), cT27('17:00'), '75bdefed-efbb-48fa-a31f-400e5972482e', {
    image: 'jonas.png',
    description: [
      'Jonas Jansson är en gitarrist och kompositör som rör sig mellan östgötsk, värmländsk, irländsk och egenskriven folkmusik. Han är utbildad vid Musikhögskolan Ingesund och har studerat för traditionsbärare som Mats Berglund, Mattias Perez, Lisa Hellsten och Jennikel Andersson. Vid sidan av sitt arbete som musiklärare är han aktiv som musiker i Fredagsduon, Trio Fri, Kongshaug/Jansson Duo, Östgöta Irish, som solomusiker och med sitt musikteatraliska soloprojekt Ticstrafik, Folkmusik och Gitarrtragik.',
    ],
    hideHeader: false,
    links: [
      cLInstagram('https://www.instagram.com/jonasjanssonmusik/'),
      cLFacebook('https://facebook.com/JonasJanssonmusik/'),
      cLYoutube('https://youtube.com/channel/UCaXq3ut-dZsiySZadIzrUBA'),
      cLOther('https://jonasjanssonmusik.com/'),
    ],
  }),
  mAct('Malin Sjöberg', cT27('17:00'), cT27('17:45'), '221728b3-0357-43b3-b0fa-edb3052efd14', {
    image: 'malin.png',
    description: [
      'Under Kulturnatten tolkar Malin Sjöberg och Haris Kapidzic Taylor Swift låtar från olika eror (album). I våras spelade de ihop under en hyllningskonsert till Taylor Swift i samarbete med Musikens Hus och har därefter börjat giga tillsammans under bröllop, privata fester samt events.',
      'Malin släpper en EP i jazz-anda senare i höst och skriver även musik inom country, ett flertal av hennes låtar har genom åren spelats i P4. Haris är gitarrist sedan 26 år tillbaka.',
    ],
    hideHeader: false,
    links: [cLInstagram('https://www.instagram.com/malinsjobergmusic/')],
  }),
  mAct('Louise & Simon', cT27('17:45'), cT27('18:15'), '53b48e26-1a57-4a96-967d-3615a577a21e'),
  mAct('Alma Ångman', cT27('19:15'), cT27('19:45'), '086b5da6-b2bd-4ed4-ad5e-fae75fa5ef61', {
    image: 'alma-angman.png',
    hideHeader: false,
    links: [
      cLInstagram('https://www.instagram.com/alma_angman'),
      cLYoutube('https://www.youtube.com/@alma_angman'),
    ],
    description: [
      'Tjejen med en personlighet lika stor som hennes mäktiga röst! Hon sjunger allt från popklassiker till 60-talsrock och blues. ',
      'På scenen har hon med sig några fantastiska musiker: Dennis Wenger, som groovar på bas och sjunger med henne, David Richardson, en pianomästare som alltid imponerar, och den själsfulla gitarristen Ola Palmkvist. ',
      'Det är ett musikaliskt drömlag du inte vill missa!',
    ],
  }),
  mAct(
    'Kongshaug/ Jansson/ Andersson',
    cT27('20:30'),
    cT27('21:15'),
    'f51eada9-0c9b-4a90-885f-72da7f9f9668',
    {
      image: 'folkmusic-group.png',
      description: [
        'Kongshaug/Andersson/Jansson skapar musik med influenser från visa, folk, jazz och akustisk pop. Trion bjuder på egenskriven musik och utlovar tänkvärda texter, vackra melodier och kompetent samspel. Trion består av jazzmusikern Isabelle Kongshaug (sång, mandolin, fiol, gitarr), singer/songwriter Magdalena Andersson (sång, tvärflöjt) och folkmusikern Jonas Jansson (oktavmandolin, gitarr). Kom och njut av fin stämsång och ösigt gitarrspel (och diverse andra instrument)!',
      ],
      hideHeader: false,
    },
  ),

  rAct(
    'Richardsons Orkester',
    cT27('18:15'),
    cT27('19:15'),
    'eaf9c6a8-4c38-4538-8f77-bd1fe852559b',
  ),
  rAct('THE ACES', cT27('19:45'), cT27('19:53'), 'f61e56b9-8b62-48d9-b51e-452438234c1c', {
    image: 'the-aces.png',
    description: [
      'Vi är en dansgrupp från Norrköping som älskar att stå på scen och showa. Vi är alltid redo för ett nytt gigg eller projekt och vi är supertaggade att komma och dansa på Kulturnatten i Knäppingsborg!',
    ],
    links: [cLInstagram('https://www.instagram.com/theaces_nrkpg')],
    hideHeader: false,
  }),
  rAct('Solaris', cT27('19:55'), cT27('20:30'), '9bafa431-0d6d-426f-9552-f51a8b657571', {
    description: [
      'Solaris är coverbandet som har något för alla! De bjuder på en mix av pop, rock, funk och soul som får dansgolvet att vibrera.',
      'Med energi och skicklighet levererar de hits från alla dina favoritgenrer, perfekt för varje fest!',
    ],
    image: 'solaris.png',
    links: [cLInstagram('https://www.instagram.com/solaris.nkpg/')],
  }),
  rAct(
    'Simon Lindström med band',
    cT27('21:30'),
    cT27('22:15'),
    'fb10efc9-c7db-4420-83cb-662fcbbab320',
    {
      image: 'simon.png',
      description: [
        'Simon Lindström kommer från en tätort utanför Linköping och under de senaste åren har han varit en del av den nya musikscenen i Linköping.\nHans musik är en blandning av egen pop/rock, med andra ord en artist som skapar en ny stil av indie/pop-rock från den vackra slätten i Östergötland.\nDebuten ägde rum med låten ”Ta en sväng med mig runt stan".\nEnligt recensenter och skribenter är Lindström prinsen av Linkan och har gjort en fantastisk resa för både publik och kulturliv.',
        'Simon har även medverkat som förband åt Lars Winnerbäck under Nyponstiftelsen år 2022.',
        'Lindström vann även årets upplaga av P4 nästa Östergötland. Sedan dess har det blivit en flytt till Göteborg, är snart aktuell med ny musik, och jobbar just nu på sin debutplatta.',
      ],
      hideHeader: false,
      links: [
        cLInstagram('https://www.instagram.com/s.lindstrom/'),
        cLFacebook('https://www.facebook.com/simonlindstrom'),
      ],
    },
  ),
  rAct(
    'Jake & the Slick Dudes',
    cT27('22:30'),
    cT28('00:00'),
    '0aa02bad-bdc6-400d-bed8-be000f4b3dbd',
    {
      image: 'jake-and-the-slick-dudes.png',
      links: [
        cLFacebook('https://www.facebook.com/jakeandtheslickdudes'),
        cLYoutube('https://www.youtube.com/user/JHuddenMusic'),
        cLOther('https://www.jhmusic.se/'),
      ],
      description: [
        'Det här är bandet som tar dig med på en svängig resa genom tidlösa klassiker från den amerikanska Westcoast-katalogen!',
        'Oavsett om du vill gunga med till gamla favoriter eller upptäcka nya guldkorn, är Jake och hans Slick Dudes redo att leverera en oförglömlig musikupplevelse med massor av stil och charm!',
      ],
    },
  ),

  bAct('Leion', cT27('17:00'), cT27('17:45'), '9d19984f-ce05-40f9-aa36-f1827b282c0e', {
    image: 'leion.png',
    description: [
      'Trubaduren som utmärker sig med sitt rappa gitarrspel och pumpande bastrumma. Under Kulturnatten blandas eget vispopigt material med några väl utvalda covers i folkrockig tappning.',
    ],
    links: [
      cLInstagram('https://www.instagram.com/joakimleion/'),
      cLYoutube('https://www.youtube.com/watch?v=x9deen6oWW4'),
      cLOther('https://leionmusic.com/'),
    ],
  }),
  bAct(
    'Kongshaug/ Jansson/ Andersson',
    cT27('18:15'),
    cT27('19:15'),
    '2cf79703-b89c-42bf-bbb0-3c322993accf',
    {
      image: 'folkmusic-group.png',
      description: [
        'Kongshaug/Andersson/Jansson skapar musik med influenser från visa, folk, jazz och akustisk pop. Trion bjuder på egenskriven musik och utlovar tänkvärda texter, vackra melodier och kompetent samspel. Trion består av jazzmusikern Isabelle Kongshaug (sång, mandolin, fiol, gitarr), singer/songwriter Magdalena Andersson (sång, tvärflöjt) och folkmusikern Jonas Jansson (oktavmandolin, gitarr). Kom och njut av fin stämsång och ösigt gitarrspel (och diverse andra instrument)!',
      ],
      hideHeader: false,
    },
  ),
  bAct('Nelle', cT27('19:30'), cT27('20:15'), '05d80ede-e9c3-4c49-b457-f788e99364f0', {
    image: 'nelle.png',
    links: [
      cLInstagram('https://www.instagram.com/n.e.l.l.e.n'),
      cLFacebook('https://www.facebook.com/nellen.musik'),
      cLYoutube('https://youtube.com/channel/UCd6kxWhPM5ZKu_QXEtSP40g'),
    ],
    description: [
      'Nelle skapar akustisk pop med en touch av melankoli. Hennes senaste singel "jag vet" släpptes i samband med tävlingen P4 Nästa Östergötland, där hon var en av årets finalister. Tillsammans med singer/songwriter Isabelle Kongshaug (kör, gitarr, mandolin) bjuder hon under Kulturnatten på låtar ur sin egen katalog - både släppta och osläppta. Det blir avskalat, avslappnat och ärligt.',
    ],
  }),

  mAct('Invigning', cT28('12:15'), cT28('12:30'), 'c7ee5bbe-f680-4d3d-85b6-e486a5c828a5'),
  mAct(
    'Sångelever från Musikens hus',
    cT28('12:30'),
    cT28('13:00'),
    'd272b6e9-537a-4098-aad8-1ccf34fefdab',
  ),
  mAct(
    'Olle, Alice & Lasse',
    cT28('13:00'),
    cT28('13:30'),
    '75b0f160-ec46-4df9-8522-b0f7fb1b2f6f',
    {
      description: [
        'Vi är Alice Brogren, Olle Petersson och Lasse Petersson. En musikertrio som spelat ihop ett tag nu. Vi kör allt från svenska visor till amerikanska powerduetter och mycket mer. Vi ser fram emot att uppträda under Kulturnatten i Knäppingsborg!',
      ],
      image: 'olle-alice-lasse.png',
    },
  ),
  mAct('Nostalgeeks', cT28('13:30'), cT28('14:00'), '2b18c67a-1864-4e77-84a4-d374ab9a1c83', {
    image: 'nostalgeeks.png',
    links: [
      cLFacebook('https://www.facebook.com/theNostalGeeks/'),
      cLOther('https://www.thenostalgeeks.se/'),
    ],
    description: [
      'Vi är en blandad kör med en bred repertoar under ledning av David Richardson.',

      'The Nostalgeeks startade 2010 i samband med att Lisa Tilling anordnade körtävlingen ”Kördraget” som vi vann!',

      'Vår repertoar är i princip all sorts musik. Vi sjunger på dop, bröllop, firmafester, gudstjänster i kyrkor, jubileumsevenemang, födelsedagar och i många fler sammanhang.',
    ],
  }),
  mAct('Trubadur-SM', cT28('14:30'), cT28('16:15'), '746d55cb-16f0-4dfb-a6c1-d7fcb1e86b42', {
    description: [
      'Vi arrangerar Trubadur-SM 2024!',
      'Under Kulturnatten gör vi repris på förra årets succe. Den lilla tävlingen med glimten i ögat. Tanken är helt enkelt att samla trubadurer och underhållare i Östergötland för att göra upp om vem som vinner Trubadur-SM 2024! Publiken får vara med och rösta och vi utlovar några av länets bästa underhållare!',
    ],
  }),
  mAct('Triss i Ess', cT28('16:30'), cT28('17:15'), 'f973b99f-4a6f-42e5-8cc1-bd61a91b1bdf', {
    image: 'triss-i-ess.png',
    links: [cLFacebook('https://www.facebook.com/trissnkpg/')],
  }),
  mAct('Prisutdelning', cT28('17:15'), cT28('17:45'), 'be65759e-5fd3-4db3-91e4-55e3ef3b8982'),
  mAct(
    'Lena "Elle" Lindström',
    cT28('18:30'),
    cT28('19:00'),
    '83fb35f2-6a6f-4cfa-97d5-646fc7585058',
    {
      description: [
        'Lena Elle är en sångerska, musikalartist och låtskrivare med en passion för att sprida glädje genom sin musik.',
        'Med en talang för att skriva musik som berör och lyfter stämningen, skapar hon låtar som alla kan njuta av och bli glada av.',
        'Lena Elle är en artist som verkligen älskar att skapa musik för hjärtat!',
      ],
      image: 'lenaelle.png',
      links: [
        cLInstagram('https://www.instagram.com/lenaellemusic/'),
        cLYoutube('https://www.youtube.com/@jordgubbslena'),
        cLFacebook('https://sv-se.facebook.com/lena.lindstrom17/'),
      ],
    },
  ),
  mAct('Ola Palmkvist', cT28('19:00'), cT28('19:45'), 'e0fc569a-cdb5-432a-88e3-d8d5b374ea6e', {
    image: 'olapalmkvist.png',
    description: [
      'Ola Palmkvist, trubaduren som alltid får dig att le! Med sin gitarr i handen och sin varma röst förtrollar han publiken och sprider glädje genom varje ton han spelar och varje sång han sjunger.',
    ],
    links: [cLInstagram('https://www.instagram.com/ola_palmkvist/')],
  }),
  mAct('Hjärter7', cT28('20:30'), cT28('21:15'), 'd19f49e7-62ff-43a8-ace0-e9fb573ae88b', {
    image: 'hjarter-7.png',
    links: [cLInstagram('https://www.instagram.com/hjaarter7/')],
    description: [
      'Duon Hjärter7 består av två hjärtan som blev till ett! ❤️',
      'Med en varm och harmonisk stil bjuder de på glada rytmer och stämningsfull musik som sträcker sig från jazz och blues till indie och pop.',
      'Oavsett om du vill sväva bort i sköna melodier eller gunga med till upplyftande beats, sprider Hjärter7 alltid värme och kärlek genom sin musik.',
    ],
  }),
  mAct('Leion', cT28('22:40'), cT28('23:10'), 'b796af1b-69a9-48f7-98a5-5911c8ad145a', {
    image: 'leion.png',
    description: [
      'Trubaduren som utmärker sig med sitt rappa gitarrspel och pumpande bastrumma. Under Kulturnatten blandas eget vispopigt material med några väl utvalda covers i folkrockig tappning.',
    ],
    links: [
      cLInstagram('https://www.instagram.com/joakimleion/'),
      cLYoutube('https://www.youtube.com/watch?v=x9deen6oWW4'),
      cLOther('https://leionmusic.com/'),
    ],
  }),

  rAct('We come in peace', cT28('17:45'), cT28('18:30'), '6f8e08b2-fbda-4a12-ae50-fc5ba1b9c6ea', {
    description: [
      'We Come In Peace spelar egen mångsidig musik som befinner sig i landet runt alternativ rock, jazz och artrock där synthtunga landskap brottas med komplexa rytmer.',
      'Therese Klenfeldt - Sång & Synth\nErik Skott - Gitarr & Synth\nHenric Bellinger - Gitarr\nPontus Holmberg - Piano & Synth\nAxel Nyström - Bas\nJoakim Sandén - Trummor & Pad',
    ],
    image: 'we-come-in-peace.png',
    links: [cLFacebook('https://www.facebook.com/wciptheband')],
  }),
  rAct('Synkopter', cT28('19:45'), cT28('20:30'), '99d48254-fb64-4c26-8719-0d46c54434d4'),
  rAct('BRÄRS', cT28('21:15'), cT28('22:00'), 'c7ff4e6c-fd97-44fd-9281-e8dc15856749', {
    description: [
      'Nystartad och väletablerad orkester som tolkar nutida pop och techno på blåsinstrument. Förbered er på att att höra era favoritlåtar framförda som de skulle ha spelats av ett 100-mannabrassband på steroider.',
    ],
    image: 'brars.png',
    links: [cLInstagram('https://www.instagram.com/brars_band'), cLOther('https://brars.se/')],
  }),
  rAct('Femme Fusion', cT28('22:10'), cT28('22:30'), '3f720cad-b5ba-4a4e-867a-6f1f4c765f1e', {
    description: [
      'Femme Fusion är en showgrupp som erbjuder dans och sång med en tydlig inspiration av burlesque, en konstform som kombinerar sensualitet, humor och extravagans.',
    ],
    links: [
      cLInstagram('https://instagram.com/femmefusionofficial'),
      cLOther('https://femmefusion.se'),
    ],
    image: 'femme-fusion.png',
  }),
  rAct('Amanda & David', cT28('22:30'), cT28('23:30'), 'fa943f82-32b2-49e5-8182-83dc255eeb1f', {
    image: 'amanda-and-david.jpg',
    description: [
      'Tillsammans har Amanda och David under de senaste åren spelat på flertalet företagsevent, privatfester, restauranger och krogar runt om i Östergötland. De är en dynamisk och sammansvetsad duo med en bred repertoar.',
    ],
  }),
  rAct('Wenger Leion', cT29('00:00'), cT29('01:00'), 'f4e3943f-b66b-4234-bb9c-f27b87061880', {
    image: 'wengerleion.png',
    links: [
      cLInstagram('https://www.instagram.com/wengerleion/'),
      cLFacebook('https://web.facebook.com/wengerleion/'),
    ],
    description: [
      'Wenger Leion är en duo som levererar stor musik med liten uppsättning. Med Dennis på sång, elbas och virveltrumma, och Joakim på sång, gitarr och bastrumma, skapar de ett fylligt ljud som trotsar sin minimala uppsättning. Deras harmoniska sång och samspel bjuder på medryckande tolkningar av rock- och popklassiker från 60-talet till idag.',
      'Med en bred repertoar av tidlösa hits skapar de en oförglömlig feststämning. Kom och upplev en kväll fylld av energi – det är omöjligt att stå still!'
    ]
  }),

  bAct('Ola Palmkvist', cT28('17:00'), cT28('17:45'), '7c9b1652-5bde-4b9b-998d-d93bc5e30322', {
    image: 'olapalmkvist.png',
    description: [
      'Ola Palmkvist, trubaduren som alltid får dig att le! Med sin gitarr i handen och sin varma röst förtrollar han publiken och sprider glädje genom varje ton han spelar och varje sång han sjunger.',
    ],
    links: [cLInstagram('https://www.instagram.com/ola_palmkvist/')],
  }),
  bAct('R.S.L', cT28('18:15'), cT28('19:00'), 'af8e34a9-ec1e-437e-902f-3535b02e839b', {
    image: 'r-s-l.png',
    description: [
      'R.S.L är ett indieband från Norrköping som bildades i slutet av 2019.',
      'Sedan starten har bandet gjort flera spelningar både runt om i Östergötland, samt i Stockholm och har även agerat support åt Terra.',
      'Under 2021 släppte R.S.L flera singlar som även spelats på radio.',
      'Musiken kan beskrivas som melodiös indierock med inslag av energisk pop där både melankoli och eufori får utrymme att mötas.',
    ],
    links: [cLFacebook('https://web.facebook.com/p/RSL-100063576910244/')],
  }),
  bAct(
    'Lars Boquist & Kristina Rydgren',
    cT28('19:30'),
    cT28('20:15'),
    '0f56cc82-9ff3-4c16-8d2f-58e9280f5c26',
    {
      image: 'lars-kristina.png',
    },
  ),
  bAct('Örni', cT28('20:45'), cT28('21:30'), '3d6ad47e-7287-43ef-9f85-22769c83236e', {
    image: 'orni.jpg',
    links: [
      cLFacebook('https://m.facebook.com/100086436221555/'),
      cLInstagram('https://www.instagram.com/bandet_orni'),
      cLYoutube('https://www.youtube.com/channel/UC2KV3yJi7mE-eEyfpPMuvgw'),
    ],
  }),
  bAct('Emi Li', cT28('22:00'), cT28('22:45'), '5f44d5c8-9c81-47ab-9637-6c22bb3e9cc4', {
    image: 'emili.png',
    description: [
      'Popstjärnan Emi Li! 2018 tog hon hem vinsten i P4 Nästa i Östergötland och har sedan dess gjort raketkarriär både i Sverige och utomlands.',

      'Emi Li fortsätter att ta världen med storm och vi är här för varje steg av resan!',
    ],
    links: [
      cLInstagram('https://www.instagram.com/emiliehoijerofficial/'),
      cLFacebook('https://www.facebook.com/EmilieHoijerOfficial/'),
    ],
  }),
  bAct('Almas Läte', cT28('23:15'), cT29('00:15'), '01fb6507-43fb-42f5-b97b-91e1283d586f', {
    image: 'almas-late.png',
    links: [
      cLFacebook('https://www.facebook.com/livespelning/'),
      cLOther('https://almaslate.se/index.htm'),
    ],
  }),
]

export { acts }
export type { Act }
