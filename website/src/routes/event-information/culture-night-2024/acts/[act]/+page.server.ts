import { acts } from '../../act-data';
import { error } from '@sveltejs/kit';

export const load = async ({ params }) => {
  const { act: actId } = params;

  const act = acts.find(act => act.id === actId);


  if (!act) {
    throw error(404, 'Act not found');
  }

  return {
    act
  };
};
