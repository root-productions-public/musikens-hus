type Feature = {
	name: string
	networker: boolean
	creator: boolean
	description: string
	link?: string
}

const features: Feature[] = [
	{
		name: '24/7 tillgång',
		description: 'Du kan dygnet runt, varje dag, komma in i Musikens hus.',
		networker: true,
		creator: true,
	},
	{
		name: 'Medlemsträffar',
		description:
			'Varje månad håller vi i träffar då det bjuds på dryck och tilltugg, ofta med en öppen scen eller med ett speciellt tema.',
		networker: true,
		creator: true,
	},
	{
		name: 'Kontorsplatser',
		description:
			'Det finns flera kontorsplatser i huset, de flesta med höj- och sänkbara bord. Slå dig ned, ta en kaffe, jobba, samverka och inspireras av andra!',
		networker: true,
		creator: true,
	},
	{
		name: 'Mötesrum',
		description: 'Om du behöver ta ett möte så finns rum med dämpad akustik att tillgå.',
		networker: true,
		creator: true,
	},
	{
		name: 'Light studio',
		description:
			'Du har fri tillgång till vår light studio, med enkel utrustning för att kunna spela in och producera.',
		networker: true,
		creator: true,
	},
	{
		name: 'Premium studio',
		description: 'Du har fri tillgång till våra exklusiva studiorum och kreativa rum.',
		networker: false,
		creator: true,
	},
	{
		name: 'Videoinspelning',
		description: 'Vi har regelbundet bokningsbara tillfällen för videoinspelning.',
		networker: false,
		creator: true,
	},
]

export { type Feature, features }
