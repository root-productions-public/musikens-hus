import type { Feature } from './features'

const features: Feature[] = [
	{
		name: '24/7 tillgång',
		description: 'Du kan dygnet runt, varje dag komma in i Musikens hus.',
		networker: true,
		creator: true,
	},
	{
		name: 'Medlemsträffar',
		description:
			'Varje månad har vi medlemsträffar där vi hittar på olika saker, t.ex. musikjam eller lär oss instrument',
		networker: true,
		creator: true,
	},
	{
		name: 'Kontorsplatser',
		description:
			'Det finns flera kontorsplatser med höj- och sänkbara bord. Här kan man ta med sig datorn, ta en kaffe och jobba eller nätverka.',
		networker: true,
		creator: true,
	},
	{
		name: 'Mötesrum',
		description: 'Om du behöver ta ett möte så finns rum med dämpad akustik att tillgå',
		networker: true,
		creator: true,
	},
	{
		name: 'Premium studio, rabatt',
		description: 'Du kan boka ett av våra premium studiorum till rabatterat pris',
		networker: true,
		creator: true,
	},
	{
		name: 'PA i Knäppingsborg',
		description: 'Du kan låna PA för att göra event i Knäppingsbord',
		networker: true,
		creator: true,
	},
	{
		name: 'Videoinspelning',
		description: 'Vi har regelbundet bokningsbara tillfällen för videoinspelning',
		networker: false,
		creator: true,
	},
	{
		name: 'Basic studio, obegränsat',
		description: 'Du kan boka vårt basic studiorum',
		networker: false,
		creator: true,
	},
	{
		name: 'Premium studio, obegränsat',
		description:
			'Du kan alltid tillgå ett av våra studiorum, om de inte redan är bokade förstås! 😁',
		networker: false,
		creator: true,
	},
	{
		name: 'PA var som helst',
		description: 'Du kan låna PA till egna event, var som helst.',
		networker: false,
		creator: true,
	},
]

export { type Feature, features }
