import { goto } from '$app/navigation'
import type { Page } from '@sveltejs/kit'
import { z } from 'zod'

const queryParamKey = 'is-side-panel-open'

const sidePanelSchema = z.coerce.boolean().default(false)

const replaceStateWithQuery = (values: Record<string, string | undefined>, url: URL) => {
	const { searchParams } = url

	for (const [key, value] of Object.entries(values)) {
		value
			? searchParams.set(encodeURIComponent(key), encodeURIComponent(value))
			: searchParams.delete(key)
	}

	// goto(url.pathname + '?' + searchparams.tostring(), {
	//   keepFocus: true,
	//   replaceState: true,
	//   noScroll: true,
	// });

	return url.pathname + '?' + searchParams.toString()
}

const urlWithSidePanelChange = (url: string, value: boolean) => {
  console.log('url', url)

	return replaceStateWithQuery({ [queryParamKey]: value ? 'true' : 'false' }, new URL(url))
}

export { urlWithSidePanelChange }
