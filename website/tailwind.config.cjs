const config = {
	content: [
		'./src/**/*.{html,js,svelte,ts}',
		'./node_modules/flowbite-svelte/**/*.{html,js,svelte,ts}',
		'./node_modules/flowbite-svelte-icons/**/*.{html,js,svelte,ts}'
	],

	theme: {
		extend: {
			colors: {
				blue: {
					100: 'rgb(var(--blue-10) / <alpha-value>)',
					200: 'rgb(var(--blue-20) / <alpha-value>)',
					300: 'rgb(var(--blue-30) / <alpha-value>)',
					400: 'rgb(var(--blue-40) / <alpha-value>)',
					500: 'rgb(var(--blue-50) / <alpha-value>)',
					600: 'rgb(var(--blue-60) / <alpha-value>)',
					700: 'rgb(var(--blue-70) / <alpha-value>)',
					800: 'rgb(var(--blue-80) / <alpha-value>)',
					900: 'rgb(var(--blue-90) / <alpha-value>)'
				},
				gray: {
					100: 'rgb(var(--gray-10) / <alpha-value>)',
					200: 'rgb(var(--gray-20) / <alpha-value>)',
					300: 'rgb(var(--gray-30) / <alpha-value>)',
					400: 'rgb(var(--gray-40) / <alpha-value>)',
					500: 'rgb(var(--gray-50) / <alpha-value>)',
					600: 'rgb(var(--gray-60) / <alpha-value>)',
					700: 'rgb(var(--gray-70) / <alpha-value>)',
					800: 'rgb(var(--gray-80) / <alpha-value>)',
					900: 'rgb(var(--gray-90) / <alpha-value>)'
				},
				red: {
					100: 'rgb(var(--red-10) / <alpha-value>)',
					200: 'rgb(var(--red-20) / <alpha-value>)',
					300: 'rgb(var(--red-30) / <alpha-value>)',
					400: 'rgb(var(--red-40) / <alpha-value>)',
					500: 'rgb(var(--red-50) / <alpha-value>)',
					600: 'rgb(var(--red-60) / <alpha-value>)',
					700: 'rgb(var(--red-70) / <alpha-value>)',
					800: 'rgb(var(--red-80) / <alpha-value>)',
					900: 'rgb(var(--red-90) / <alpha-value>)'
				},
				yellow: {
					100: 'rgb(var(--yellow-10) / <alpha-value>)',
					200: 'rgb(var(--yellow-20) / <alpha-value>)',
					300: 'rgb(var(--yellow-30) / <alpha-value>)',
					400: 'rgb(var(--yellow-40) / <alpha-value>)',
					500: 'rgb(var(--yellow-50) / <alpha-value>)',
					600: 'rgb(var(--yellow-60) / <alpha-value>)',
					700: 'rgb(var(--yellow-70) / <alpha-value>)',
					800: 'rgb(var(--yellow-80) / <alpha-value>)',
					900: 'rgb(var(--yellow-90) / <alpha-value>)'
				}
			}
		}
	},

	plugins: [require('flowbite/plugin')],
	darkMode: 'class'
}

module.exports = config
