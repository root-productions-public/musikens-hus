import { sveltekit } from '@sveltejs/kit/vite'
import { enhancedImages } from '@sveltejs/enhanced-img'
import type { UserConfig } from 'vite'
import path from 'path'

const config: UserConfig = {
	plugins: [enhancedImages(), sveltekit()],
	resolve: {
		alias: {
			$assets: path.resolve(__dirname, './src/assets'),
		},
	},
	// ssr: {
	// 	noExternal: ['devalue']
	// }
}

export default config
